﻿using Bogus;
using System;
using System.Collections.Generic;
using ProjectStructure.DAL.Enums;
using ProjectStructure.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context.EntityTypeConfigurations;

namespace ProjectStructure.DAL.Context
{
    public static class ModelBuilderExtensions
    {

        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProjectConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new TeamConfiguration());
            modelBuilder.ApplyConfiguration(new TaskConfiguration());
        }


        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GenerateTeams(5);
            var users = GenerateUsers(30, teams);
            var projects = GenerateProjects(10, teams, users);
            var tasks = GenerateTasks(70, projects, users);

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }


        private static ICollection<Team> GenerateTeams(int teamAmount)
        {
            int teamId = 1;

            var teamFake = new Faker<Team>()
                .RuleFor(p => p.Id, f => teamId++)
                .RuleFor(p => p.Name, f => f.Commerce.Product())
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(DateTime.Parse("2016-01-01T00:00:00.00"), DateTime.Now));

            return teamFake.Generate(teamAmount);
        }


        private static ICollection<User> GenerateUsers(int userAmount, ICollection<Team> teams)
        {
            int userId = 1;

            var userFake = new Faker<User>()
                .RuleFor(p => p.Id, f => userId++)
                .RuleFor(p => p.FirstName, f => f.Person.FirstName)
                .RuleFor(p => p.LastName, f => f.Person.LastName)
                .RuleFor(p => p.Email, f => f.Person.Email)
                .RuleFor(p => p.RegisteredAt, f => f.Date
                    .Between(DateTime.Parse("2016-01-01T00:00:00.00"), DateTime.Parse("2021-01-01T00:00:00.00")))
                .RuleFor(p => p.BirthDay, f => f.Date
                    .Between(DateTime.Parse("1960-01-01T00:00:00.00"), DateTime.Parse("2013-01-01T00:00:00.00")))
                .RuleFor(p => p.TeamId, f => f.PickRandom(teams).Id);

            return userFake.Generate(userAmount);
        }


        private static ICollection<Project> GenerateProjects(int projectAmount, ICollection<Team> teams, ICollection<User> users)
        {
            int projectId = 1;

            var fakeProject = new Faker<Project>()
                .RuleFor(p => p.Id, f => projectId++)
                .RuleFor(p => p.Name, f => f.Lorem.Sentence(6))
                .RuleFor(p => p.Description, f => f.Lorem.Sentence(15))
                .RuleFor(p => p.Deadline, f => f.Date
                    .Between(DateTime.Now, DateTime.Parse("2023-01-01T00:00:00.00")))
                .RuleFor(p => p.CreatedAt, f => f.Date
                    .Between(DateTime.Parse("2017-01-01T00:00:00.00"), DateTime.Parse("2021-01-01T00:00:00.00")))
                .RuleFor(p => p.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(p => p.TeamId, f => f.PickRandom(teams).Id);

            return fakeProject.Generate(projectAmount);
        }


        private static ICollection<Task> GenerateTasks(int taskAmount, ICollection<Project> projects, ICollection<User> users)
        {
            int taskId = 1;

            var fakeTask = new Faker<Task>()
                .RuleFor(p => p.Id, f => taskId++)
                .RuleFor(p => p.Name, f => f.Lorem.Sentence(5))
                .RuleFor(p => p.Description, f => f.Lorem.Text())
                .RuleFor(p => p.State, f => (TaskState)f.Random.Int(0, 3))
                .RuleFor(p => p.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(p => p.PerformerId, f => f.PickRandom(users).Id)
                .RuleFor(p => p.CreatedAt, f => f.Date
                    .Between(DateTime.Parse("2017-01-01T00:00:00.00"), DateTime.Parse("2021-01-01T00:00:00.00")))

                .RuleFor(p => p.FinishedAt, f =>
                {
                    if (f.Random.Int(0, 10) > 4)
                    {
                        return f.Date.Between(DateTime.Parse("2018-01-01T00:00:00.00"), DateTime.Now);
                    }
                    else
                    {
                        return null;
                    }
                });

            return fakeTask.Generate(taskAmount);
        }
    }
}
