﻿using ProjectStructure.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ProjectStructure.DAL.Context.EntityTypeConfigurations
{
    public sealed class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(p => p.FirstName)
               .IsRequired()
               .HasColumnType("varchar(64)");


            builder.Property(p => p.LastName)
                .IsRequired()
                .HasColumnType("varchar(64)");


            builder.Property(p => p.Email)
                .IsRequired()
                .HasColumnType("varchar(64)");


            builder.Property(p => p.RegisteredAt)
                .IsRequired()
                .HasColumnType("datetime2(2)")
                .HasDefaultValueSql("GETDATE()");


            builder.Property(p => p.BirthDay)
                .IsRequired()
                .HasColumnType("datetime2(2)");


            builder.HasOne(u => u.Team)
                .WithMany(t => t.Members)
                .HasForeignKey(u => u.TeamId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
