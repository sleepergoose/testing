﻿using Xunit;
using System;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Abstract;

namespace ProjectStructure.BLL.Tests
{
    public class ProjectServiceTests : BaseTestClass
    {
        private readonly ProjectService _projectService;

        public ProjectServiceTests()
        {
            _projectService = new ProjectService(_context, _mapper);
        }


        [Fact]
        public async Task AddProject_WhenAddProject_ThenThereAreTwoProjectsAndCorrespondedBody()
        {
            var projectDto = new ProjectDTO
            {
                AuthorId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now,
                Description = "Project description",
                Name = "ProjectName",
                TeamId = 1
            };

            var createdProject = await _projectService.AddProjectAsync(projectDto);


            Assert.Equal(2, (await _projectService.GetProjectsAsync() as ICollection<ProjectDTO>).Count);

            Assert.Equal(projectDto.Name, createdProject.Name);
            Assert.Equal(projectDto.Description, createdProject.Description);
            Assert.Equal(projectDto.TeamId, createdProject.TeamId);
            Assert.Equal(projectDto.AuthorId, createdProject.AuthorId);
            Assert.Equal(projectDto.CreatedAt, createdProject.CreatedAt);
            Assert.Equal(projectDto.Deadline, createdProject.Deadline);

            await _projectService.DeleteProjectAsync(createdProject.Id);
        }


        [Fact]
        public async Task UpdateProject_WhenUpdateProject_ThenCorrespondedBody()
        {
            var projectDto = new ProjectDTO
            {
                Id = 1,
                AuthorId = 1,
                CreatedAt = DateTime.Parse("2000-01-01T00:00:00.00"),
                Deadline = DateTime.Parse("2001-01-01T00:00:00.00"),
                Description = "Project description",
                Name = "Second Project",
                TeamId = 1
            };


            var updatedProject = await _projectService.UpdateProjectAsync(projectDto);

            Assert.Equal(projectDto.CreatedAt, updatedProject.CreatedAt);
            Assert.Equal(projectDto.Deadline, updatedProject.Deadline);
            Assert.Equal(projectDto.Name, updatedProject.Name);
        }


        [Fact]
        public void AddProject_WhenProjectDtoIsNull_ThenThrowAggregateException()
        {
            ProjectDTO projectDto = null;

            Assert.Throws<AggregateException>(() => _projectService.AddProjectAsync(projectDto).Result);
        }
    }
}
