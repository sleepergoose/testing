﻿using Xunit;
using System;
using System.Net;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using ProjectStructure.WebAPI.IntegrationTests.WebAppFactory;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly string _endpoint = "api/Teams";

        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        [Fact]
        public async Task Add_WhenAddTeam_ThenResponseWithCode201AndCorrespondedBody()
        {
            var teamDto = new TeamDTO
            {
                Id = 0,
                Name = "Team 1",
                CreatedAt = DateTime.Now
            };

            var teamInJson = JsonConvert.SerializeObject(teamDto);
            
            var httpResponse = await _client.PostAsync(_endpoint, new StringContent(teamInJson, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(teamDto.Name, createdTeam.Name);
            Assert.Equal(teamDto.CreatedAt, createdTeam.CreatedAt);

            await _client.DeleteAsync($"{_endpoint}/{createdTeam.Id}");
        }



        [Fact]
        public async Task Add_WhenTeamDtoIsNull_ThenResponseWithCode400()
        {
            TeamDTO projectDto = null;

            var teamInJson = JsonConvert.SerializeObject(projectDto);
            
            var httpResponse = await _client.PostAsync(_endpoint, new StringContent(teamInJson, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }



        [Theory]
        [InlineData(" {\"id\": 0,\"name\": \"\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"name\": null,\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 0,\"name\": \"TeamName\",\"createdAt\": \"time\"}")]
        [InlineData(" {\"id\": 0,\"name\": \"TeamName\",\"createdAt\": null}")]
        public async Task Add_WhenWrongRequestBody_ThenResponseCode400(string jsonInString)
        {
            var httpResponse = await _client.PostAsync(_endpoint, new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }


        [Theory]
        [InlineData(" {\"id\": 1,\"name\": \"\",\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 2,\"name\": null,\"createdAt\": \"2018-11-21T00:46:30.26\"}")]
        [InlineData(" {\"id\": 3,\"name\": \"TeamName\",\"createdAt\": \"time\"}")]
        [InlineData(" {\"id\": 4,\"name\": \"TeamName\",\"createdAt\": null}")]
        public async Task Update_WhenWrongRequestBody_ThenResponseCode400(string jsonInString)
        {
            var httpResponse = await _client.PutAsync(_endpoint, new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
