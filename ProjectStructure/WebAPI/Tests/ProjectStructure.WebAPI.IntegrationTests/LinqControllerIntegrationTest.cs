﻿using Xunit;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.WebAPI.IntegrationTests.WebAppFactory;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class LinqControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly string _url = "api/Linq";

        public LinqControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }



        [Fact]
        public async Task Linq_WhenGetUsersWithTasks_ThenUsersOrderedByFirstNameAndTasksOrderedByDescByNameLength()
        {
            var httpResponse = await _client.GetAsync($"{_url}/UsersWithTasks");
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var usersWithTasks = JsonConvert.DeserializeObject<List<UserWithTasksDTO>>(stringResponse);

            var unsortedUsers = usersWithTasks.Select(p => p.User).ToList();
            var sortedUsers = usersWithTasks.Select(p => p.User).OrderBy(u => u.FirstName).ToList();

            Assert.All(usersWithTasks, user => {
                Assert.Equal(user.Tasks.FirstOrDefault(), user.Tasks.OrderByDescending(t => t.Name.Length).FirstOrDefault());
            });

            Assert.Equal(sortedUsers.FirstOrDefault(), unsortedUsers.FirstOrDefault());
        }
    }
}
