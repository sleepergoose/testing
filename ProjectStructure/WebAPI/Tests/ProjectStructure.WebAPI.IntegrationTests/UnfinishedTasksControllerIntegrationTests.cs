﻿using Xunit;
using System;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.WebAPI.IntegrationTests.WebAppFactory;

namespace ProjectStructure.WebAPI.IntegrationTests
{
    public class UnfinishedTasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private readonly string _endpoint = "api/UnfinishedTasks";

        public UnfinishedTasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public async Task GetUnfinishedTasks_WhenPerformerHasTasks_ThenAllGottenTasksAreUnfinished(int id)
        {
            var httpResponse = await _client.GetAsync($"{_endpoint}/{id}");

            if (HttpStatusCode.OK == httpResponse.StatusCode)
            {
                var stringResponse = await httpResponse.Content.ReadAsStringAsync();

                var tasks = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

                Assert.All(tasks, task => Assert.True(task.FinishedAt == null));
            }
            else if (HttpStatusCode.NotFound == httpResponse.StatusCode)
            {
                Assert.True(true);
            }
            else
            {
                Assert.True(false);
            }
        }


        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async Task GetUnfinishedTasks_WhenWrongPerformerId_ThenBadRequest(int performerId)
        {
            var httpResponse = await _client.GetAsync($"{_endpoint}/{performerId}");

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }


        [Fact]
        public async Task GetUnfinishedTasks_WhenAddUserWithTwoUnfinishedTasks_ThenListCountIsTwo()
        {
            /* Arrange */
            // Add a new 'clear' user
            var newUserDto = new UserDTO
            {
                BirthDay = DateTime.Now,
                Email = "user@server.com",
                FirstName = "Donald",
                LastName = "Tramp",
                RegisteredAt = DateTime.Now,
                TeamId = 1,
                Id = 0
            };

            var userInJson = JsonConvert.SerializeObject(newUserDto);

            var httpResponse = await _client.PostAsync("api/Users", new StringContent(userInJson, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(stringResponse);


            // Add new unfinished tasks to user
            var newTaskDto1 = new TaskDTO
            {
                FinishedAt = null,
                CreatedAt = DateTime.Now,
                Description = "Test finished task",
                Name = "Finished task",
                PerformerId = createdUser.Id,
                ProjectId = 1,
                State = DAL.Enums.TaskState.ToDo
            };

            var newTaskDto2 = new TaskDTO
            {
                FinishedAt = null,
                CreatedAt = DateTime.Now,
                Description = "Test finished task",
                Name = "Finished task",
                PerformerId = createdUser.Id,
                ProjectId = 1,
                State = DAL.Enums.TaskState.ToDo
            };

            var taskInJson1 = JsonConvert.SerializeObject(newTaskDto1);
            var taskInJson2 = JsonConvert.SerializeObject(newTaskDto2);

            httpResponse = await _client.PostAsync("api/Tasks", new StringContent(taskInJson1, Encoding.UTF8, "application/json"));
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTask1 = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);

            httpResponse = await _client.PostAsync("api/Tasks", new StringContent(taskInJson2, Encoding.UTF8, "application/json"));
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTask2 = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);


            /* Act */
            httpResponse = await _client.GetAsync($"{_endpoint}/{createdUser.Id}");
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var unfinishedTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

            /* Assert */
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.True(unfinishedTasks.Count == 2);

            await _client.DeleteAsync($"api/Tasks/{createdTask1.Id}");
            await _client.DeleteAsync($"api/Tasks/{createdTask2.Id}");
            await _client.DeleteAsync($"api/Users/{createdUser.Id}");
        }



        [Fact]
        public async Task GetUnfinishedTasks_WhenUserHasNoUnfinishedTasks_ThenStatusCode404()
        {
            /* Arrange */
            // Add a new 'clear' user
            var newUserDto = new UserDTO
            {
                BirthDay = DateTime.Now,
                Email = "user@server.com",
                FirstName = "Donald",
                LastName = "Tramp",
                RegisteredAt = DateTime.Now,
                TeamId = 1
            };

            var userInJson = JsonConvert.SerializeObject(newUserDto);
            var httpResponse = await _client.PostAsync("api/Users", new StringContent(userInJson, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(stringResponse);

            // Add new and finished tasks to user
            var newTaskDto1 = new TaskDTO
            {
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Test finished task",
                Name = "Finished task",
                PerformerId = createdUser.Id,
                ProjectId = 1,
                State = DAL.Enums.TaskState.Done
            };

            var newTaskDto2 = new TaskDTO
            {
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Test finished task",
                Name = "Finished task",
                PerformerId = createdUser.Id,
                ProjectId = 1,
                State = DAL.Enums.TaskState.Done
            };

            var taskInJson1 = JsonConvert.SerializeObject(newTaskDto1);
            var taskInJson2 = JsonConvert.SerializeObject(newTaskDto2);

            httpResponse = await _client.PostAsync("api/Tasks", new StringContent(taskInJson1, Encoding.UTF8, "application/json"));
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTask1 = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);

            httpResponse = await _client.PostAsync("api/Tasks", new StringContent(taskInJson2, Encoding.UTF8, "application/json"));
            stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTask2 = JsonConvert.DeserializeObject<TaskDTO>(stringResponse);


            /* Act */
            httpResponse = await _client.GetAsync($"{_endpoint}/{createdUser.Id}");

            /* Assert */
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);

            await _client.DeleteAsync($"api/Tasks/{createdTask1.Id}");
            await _client.DeleteAsync($"api/Tasks/{createdTask2.Id}");
            await _client.DeleteAsync($"api/Users/{createdUser.Id}");
        }
    }
}
