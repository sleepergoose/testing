﻿using System.Reflection;
using ProjectStructure.BLL.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<ProjectProfile>();
            },
            Assembly.GetExecutingAssembly());
        }


        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<ProjectService>();
            services.AddScoped<UserService>();
            services.AddScoped<TeamService>();
            services.AddScoped<TaskService>();
            services.AddScoped<LinqService>();
            services.AddScoped<UnfinishedTaskService>();
        }
    }
}
