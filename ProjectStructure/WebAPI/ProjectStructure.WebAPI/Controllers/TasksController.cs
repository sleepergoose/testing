﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {

        private readonly TaskService _taskService;


        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Get()
        {
            return Ok(await _taskService.GetTasksAsync());
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            if (id <= 0)
                return BadRequest("Task ID cannot be less than or equal to zero");

            var task = await _taskService.GetTaskAsync(id);

            if (task == null)
                return NotFound("There are no task with this Id");

            return Ok(task);
        }


        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TaskDTO taskDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var task = await _taskService.AddTaskAsync(taskDTO);
                return CreatedAtAction(nameof(Get), new { Id = task.Id }, task);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TaskDTO taskDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var task = await _taskService.UpdateTaskAsync(taskDTO);
                return Ok(task);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (id <= 0)
                return BadRequest("Task ID cannot be less than or equal to zero");

            try
            {
                var task = await _taskService.GetTaskAsync(id);

                if (task == null)
                    return NotFound("There are no task with this Id");

                return Ok(await _taskService.DeleteTaskAsync(id));
            }
            catch (ArgumentNullException ex)
            {
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
