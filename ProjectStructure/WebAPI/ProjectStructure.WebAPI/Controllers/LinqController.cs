﻿using System.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.BLL.Services;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;


        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }


        /* -- 1 -- */
        [HttpGet("TasksAmount/{id}")]
        public ActionResult<Dictionary<ProjectDTO, int>> GetTasksAmount(int id)
        {
            if (id <= 0)
                return BadRequest("ID cannot be less than or equal to zero");

            var dict = _linqService.GetTasksAmount(id).ToList();

            return Ok(dict);
        }


        /* -- 2 -- */
        [HttpGet("TasksList/{id}")]
        public ActionResult<List<TaskDTO>> GetTasksList(int id)
        {
            if (id <= 0)
                return BadRequest("ID cannot be less than or equal to zero");

            var tasks = _linqService.GetTasksList(id);

            return Ok(tasks);
        }


        /* -- 3 -- */
        [HttpGet("FinishedTasks/{id}")]
        public ActionResult<List<(int Id, string Name)>> GetFinishedTasks(int id)
        {
            if (id <= 0)
                return BadRequest("ID cannot be less than or equal to zero");

            var tasks = _linqService.GetFinishedTasks(id);

            return Ok(JsonConvert.SerializeObject(tasks));
        }


        /* -- 4 -- */
        [HttpGet("TeamsMembers")]
        public ActionResult<List<(int Id, string TeamName, List<UserDTO> Users)>> GetTeamsMembers()
        {
            var teams = _linqService.GetTeamsMembers();

            return Ok(JsonConvert.SerializeObject(teams));
        }


        /* -- 5 -- */
        [HttpGet("UsersWithTasks")]
        public ActionResult<List<UserWithTasksDTO>> GetUsersWithTasks()
        {
            var users = _linqService.GetUsersWithTasks();

            return Ok(users);
        }


        /* -- 6 -- */
        [HttpGet("UserSummary/{id}")]
        public ActionResult<UserSummaryDTO> GetUserSummary(int id)
        {
            if (id <= 0)
                return BadRequest("ID cannot be less than or equal to zero");

            var user = _linqService.GetUserSummary(id);

            return Ok(user);
        }


        /* -- 7 -- */
        [HttpGet("ProjectSummary")]
        public ActionResult<List<ProjectSummaryDTO>> GetProjectSummary()
        {
            var projects = _linqService.GetProjectSummary();

            return Ok(projects);
        }
    }
}
