﻿using AutoMapper;
using ProjectStructure.BLL.DTO;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<ProjectStructure.DAL.Entities.Task, TaskDTO>();

            CreateMap<TaskDTO, ProjectStructure.DAL.Entities.Task>();
        }
    }
}
