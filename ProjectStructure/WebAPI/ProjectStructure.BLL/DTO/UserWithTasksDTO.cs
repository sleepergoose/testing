﻿using System.Collections.Generic;

namespace ProjectStructure.BLL.DTO
{
    public sealed class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
