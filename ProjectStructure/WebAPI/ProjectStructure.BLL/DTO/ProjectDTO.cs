﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BLL.DTO
{
    public sealed class ProjectDTO
    {
        public int Id { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int AuthorId { get; set; }

        [Range(0, int.MaxValue)]
        public int TeamId { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        [MinLength(3)]
        public string Description { get; set; }

        [Required]
        public DateTime Deadline { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }
    }
}

