﻿using System;
using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public sealed class TaskService : BaseService
    {
        public TaskService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<TaskDTO>> GetTasksAsync()
        {
            var tasks = await _context.Tasks.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }


        public async Task<TaskDTO> GetTaskAsync(int id)
        {
            var task = await _context.Tasks.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);

            return _mapper.Map<TaskDTO>(task);
        }


        public async Task<TaskDTO> AddTaskAsync(TaskDTO taskDTO)
        {
            if (taskDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                taskDTO.Id = 0;

                var task = _mapper.Map<DAL.Entities.Task>(taskDTO);

                _context.Tasks.Add(task);

                await _context.SaveChangesAsync();

                var createdTask = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == task.Id);

                return _mapper.Map<TaskDTO>(createdTask);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<TaskDTO> UpdateTaskAsync(TaskDTO taskDTO)
        {
            try
            {
                var task = _mapper.Map<DAL.Entities.Task>(taskDTO);

                _context.Tasks.Update(task);

                await _context.SaveChangesAsync();

                var updatedTask = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == task.Id);

                return _mapper.Map<TaskDTO>(updatedTask);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<int> DeleteTaskAsync(int id)
        {
            try
            {
                var deletedTask = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == id);

                _context.Tasks.Remove(deletedTask);

                await _context.SaveChangesAsync();

                return id;
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
