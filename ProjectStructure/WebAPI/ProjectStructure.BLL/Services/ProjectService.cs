﻿using System;
using AutoMapper;
using System.Threading.Tasks;
using ProjectStructure.BLL.DTO;
using System.Collections.Generic;
using ProjectStructure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Services.Abstract;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : BaseService
    {

        public ProjectService(ApplicationContext context, IMapper mapper)
            : base(context, mapper)
        { }


        public async Task<IEnumerable<ProjectDTO>> GetProjectsAsync()
        {
            var projects = await _context.Projects.AsNoTracking().ToListAsync();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }


        public async Task<ProjectDTO> GetProjectAsync(int id)
        {
            var project = await _context.Projects.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);

            return _mapper.Map<ProjectDTO>(project);
        }


        public async Task<ProjectDTO> AddProjectAsync(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                projectDTO.Id = 0;

                var project = _mapper.Map<DAL.Entities.Project>(projectDTO);

                _context.Projects.Add(project);

                await _context.SaveChangesAsync();

                var createdProject = await _context.Projects.FirstOrDefaultAsync(p => p.Id == project.Id);

                return _mapper.Map<ProjectDTO>(createdProject);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<ProjectDTO> UpdateProjectAsync(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
                throw new ArgumentNullException("Argument cannot be null");

            try
            {
                var project = _mapper.Map<DAL.Entities.Project>(projectDTO);

                _context.Projects.Update(project);

                await _context.SaveChangesAsync();

                var updatedProject = await _context.Projects.FirstOrDefaultAsync(p => p.Id == project.Id);

                return _mapper.Map<ProjectDTO>(updatedProject);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<int> DeleteProjectAsync(int id)
        {
            try
            {
                var deletedProject = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);

                _context.Projects.Remove(deletedProject);

                await _context.SaveChangesAsync();

                return id;
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
