﻿using AutoMapper;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly ApplicationContext _context;
        private protected readonly IMapper _mapper;

        public BaseService(ApplicationContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
