﻿using Client.Common.DTO;
using System.Threading.Tasks;
using Client.BLL.Services.Abstract;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Client.BLL.Services
{
    public class TaskService : BaseService
    {
        private string _url;

        public TaskService(string host) : base(host)
        {
            _url = $"{Host}/api/Tasks";
        }

        public async Task<IEnumerable<Client.Common.Entities.Task>> GetAllTasks()
        {
            var response = await _httpService.GetAsync(_url);

            var tasks = JsonConvert.DeserializeObject<ICollection<TaskDTO>>(response);

            return _mapper.Map<IEnumerable<Client.Common.Entities.Task>>(tasks);
        }


        public async Task<Client.Common.Entities.Task> GetTask(int id)
        {
            var response = await _httpService.GetAsync($"{_url}/{id}");

            var task = JsonConvert.DeserializeObject<TaskDTO>(response);

            return _mapper.Map<Client.Common.Entities.Task>(task);
        }
    }
}
