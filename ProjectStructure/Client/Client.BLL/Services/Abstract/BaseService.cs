﻿using System.Net.Http;
using AutoMapper;

namespace Client.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        protected readonly HttpService _httpService;

        protected readonly IMapper _mapper;

        public string Host { get; }


        public BaseService(string host)
        {
            Host = host;

            _httpService = new HttpService();

            var mapperConfig = new MapperConfiguration(mc => {
                mc.AddProfile(new MappingProfile());
            });

            _mapper = mapperConfig.CreateMapper();
        }
    }
}
