﻿using Client.Interfaces;

namespace Client
{
    public sealed class Presenter
    {
        delegate void Manager(string response = "");
        Manager _globalManager;

        private readonly IView _view;
        private readonly IQueries _queries;


        public Presenter(IView view, IQueries queries)
        {
            _globalManager = MainManager;
            _view = view;
            _queries = queries;
        }


        public void Run()
        {
            _view.ShowMenu();
            _globalManager();
        }

        
        private void MainManager(string command)
        {
            _view.ShowMenu();

            switch (command)
            {
                case "1":

                        int authorId;

                        if(int.TryParse(_view.GetTextResponse("Enter AuthorId (for example: 13)"), out authorId) == true)
                        {
                            var query1 = _queries.GetTasksAmount(authorId); 
                            _view.ShowTaskAmount(query1);
                        }
                        else
                        {
                            _view.WriteErrorToCenter("You entered wrong parameter");
                        }

                    break;
                case "2":

                        int performerId;

                        if (int.TryParse(_view.GetTextResponse("Enter PerformerId (for example: 3)"), out performerId) == true)
                        {
                            var query2 = _queries.GetTasksList(performerId);
                            _view.ShowTaskList(query2);
                        }
                        else
                        {
                            _view.WriteErrorToCenter("You entered wrong parameter");
                        }

                    break;
                case "3":

                        if (int.TryParse(_view.GetTextResponse("Enter PerformerId (for example: 5)"), out performerId) == true)
                        {
                            var query3 = _queries.GetFinishedTasks(performerId);
                            _view.ShowFinishedTasks(query3);
                        }
                        else
                        {
                            _view.WriteErrorToCenter("You entered wrong parameter");
                        }

                    break;
                case "4":

                        var query4 = _queries.GetTeamsMembers();
                        _view.ShowTeamsMembers(query4);
                        break;
                    case "5":
                        var query5 = _queries.GetUsersWithTasks();
                        _view.ShowUsersWithTasks(query5);

                    break;
                case "6":

                        int userId;
                        if (int.TryParse(_view.GetTextResponse("Enter PerformerId (for example: 13)"), out userId) == true)
                        {
                            var query6 = _queries.GetUserSummary(userId: userId);
                            _view.ShowUserSummary(query6);
                        }
                        else
                        {
                            _view.WriteErrorToCenter("You entered wrong parameter");
                        }

                    break;
                case "7":

                        var query7 = _queries.GetProjectSummary();
                        _view.ShowProjectSummary(query7);

                    break;
                case "exit":

                        _view.WriteTextToCenter("Good Luck!");
                        _view.GetTextResponse("...Press 'Enter' key to exit...");
                        _globalManager = null;

                    break;
             }

            _globalManager?.Invoke(_view.GetTextResponse("Enter method number or exit"));
        }
    }
}
