﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.BLL.Services;
using Client.Common.Entities;
using Client.Interfaces;

namespace Client
{
    public class DataLoader : IDataLoader
    {
        private readonly ProjectService _projectService;
        private readonly TaskService _taskService;
        private readonly TeamService _teamService;
        private readonly UserService _userService;


        public DataLoader(ProjectService projectService, TaskService taskService,
                          TeamService teamService, UserService userService)
        {
            _projectService = projectService;
            _taskService = taskService;
            _teamService = teamService;
            _userService = userService;
        }


        public async Task<List<Project>> GetDataStructure()
        {
            try
            {
                var projects = await _projectService.GetAllProgects();
                var teams = await _teamService.GetAllTeams();
                var users = await _userService.GetAllUsers();
                var tasks = await _taskService.GetAllTasks();


                projects = projects.GroupJoin(
                    tasks.Join(users, task => task.PerformerId, user => user.Id, (task, user) =>
                        {
                            task.Performer = user;
                            return task;
                        }),
                    project => project.Id, task => task.ProjectId, (project, _tasks) =>
                    {
                        project.Tasks = _tasks as ICollection<Client.Common.Entities.Task>;
                        return project;
                    })
                    .Join(users, project => project.AuthorId, user => user.Id, (project, user) =>
                    {
                        project.Author = user;
                        return project;
                    })
                    .Join(teams, project => project.TeamId, team => team.Id, (project, team) =>
                     {
                         project.Team = team;
                         return project;
                     });


                return projects.ToList();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine($"Error: {ex.Message}");
                throw new System.Exception();
            }
        }
    }
}
