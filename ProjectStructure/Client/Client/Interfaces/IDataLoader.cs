﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Client.Common.Entities;

namespace Client.Interfaces
{
    public interface IDataLoader
    {
        Task<List<Project>> GetDataStructure();
    }
}
