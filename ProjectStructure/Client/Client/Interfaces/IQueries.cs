﻿using System.Collections.Generic;
using Client.Common.Entities;

namespace Client.Interfaces
{
    public interface IQueries
    {
        List<(int Id, string Name)> GetFinishedTasks(int performerId);

        List<ProjectSummary> GetProjectSummary();

        Dictionary<Project, int> GetTasksAmount(int authorId);

        List<Client.Common.Entities.Task> GetTasksList(int performerId);

        List<(int Id, string TeamName, List<User> Users)> GetTeamsMembers();

        UserSummary GetUserSummary(int userId);

        List<User> GetUsersWithTasks();
    }
}
